# AWS IoT Device Simulator

This repo is a fork of the original AWS ELF script to simulate an IoT device. This implementation aims at simplifying the bootstrapping process so the reader can focus on the data processes and learn about the potentials of the AWS IoT service.

Original forked repo: https://github.com/aws-samples/aws-iot-elf

This repo contains supporting code for the following blog post: https://datacenternotes.com/2018/09/09/getting-started-with-an-aws-iot-with-a-dockerized-device/
