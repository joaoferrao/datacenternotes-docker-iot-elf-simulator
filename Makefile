################################################################################
# Makefile for simulating AWS IoT Extremely Low Friction device with Docker
################################################################################

# up - Launches the docker environment needed
# bootstrap

SAMPLE_FILE = radar-counts.json

up:
	@docker-compose up -d --build --force-recreate
	@docker container ls | grep elf

thing:
	@docker container exec elf /root/aws-iot-elf/elf.py create

send:
	@docker container exec elf /root/aws-iot-elf/elf.py \
		send \
		--json-message /root/aws-iot-elf/data/$(SAMPLE_FILE).json \
		--append-thing-name

bootstrap: up thing send

logs:
	@docker container logs -f elf

inside:
	@docker container exec -it elf bash

clean: down
	@docker container exec elf /root/aws-iot-elf/elf.py clean

down:
	@docker-compose down
